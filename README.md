django_statics
=============

a django app for useful static files

usage
=====

add this to you requirements.txt:

    -e git+https://bitbucket.org/nodedigital/django-statics.git#egg=django-statics

install it:

    pip install -r requirements.txt

then add `statics` to INSTALLED_APPS, and enjoy!
